-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id_article` int(11) NOT NULL AUTO_INCREMENT,
  `resume` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `nom-article` varchar(255) COLLATE utf8_bin NOT NULL,
  `pertinence` int(2) NOT NULL,
  `source` varchar(255) COLLATE utf8_bin NOT NULL,
  `date` date NOT NULL,
  `lien` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_article`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `article` (`id_article`, `resume`, `image`, `nom-article`, `pertinence`, `source`, `date`, `lien`) VALUES
(1,	'Il y\'a une faiblesse dans le moteur JavaScript d\'Avast force ce qui a obligé l\'entreprise à le désactiver.',	'images-bdd/image-faille-securite-avast.jpg',	'Une faille de sécurité dans Avast force l’éditeur à désactiver son moteur JavaScript',	3,	'journaldugeek.com',	'2020-03-14',	'https://www.journaldugeek.com/2020/03/14/une-faille-de-securite-dans-avast-force-lediteur-a-desactiver-son-moteur-javascript/'),
(2,	'',	'images-bdd/image-faille-securite-chrome.jpg',	'Sécuriser et protéger Google Chrome efficacement',	2,	'yubigeek.com',	'2020-08-19',	'https://www.yubigeek.com/securiser-proteger-google-chrome/'),
(3,	'',	'images-bdd/image-faille-securite-whatsapp.jpg',	'Une faille de sécurité découverte dans la version desktop de WhatsApp',	4,	'Siecledigital.fr',	'2020-02-10',	'https://siecledigital.fr/2020/02/10/une-faille-de-securite-decouverte-dans-la-version-desktop-de-whatsapp/'),
(4,	'',	'images-bdd/image-faille-securite-amd.jpg',	'Cyberattaque : les processeurs AMD souffrent de failles importantes',	1,	'Futura-sciences.com',	'2020-03-09',	'https://www.futura-sciences.com/tech/actualites/cybersecurite-cyberattaque-processeurs-amd-souffrent-failles-importantes-79937/'),
(5,	'Google a mis en place des fonds pour aider les chercheurs à trouver des vulnérabilités dans les moteurs de recherches JavaScript. Il y\'a jusqu\'à 5 000 dollars de financement. ',	'Images-bdd\\image-subvention-vulnerabilite-js.jpg',	'Google subventionne la recherche de vulnérabilités dans les moteurs JavaScript des navigateurs',	2,	'Zdnet',	'2020-10-04',	'https://www.zdnet.fr/actualites/google-subventionne-la-recherche-de-vulnerabilites-dans-les-moteurs-javascript-des-navigateurs-39910673.htm');

-- 2020-11-19 10:51:54
