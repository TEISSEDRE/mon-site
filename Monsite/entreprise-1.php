<?php
require 'inc/header.php';
?>

<h3 class="titre-presentation-1-annee"> Presentation de l'entreprise de ma 1 ère année </h3>

<a href="https://mapadirect.fr/">
<img class="image-mapadirect" src="https://mapadirect.fr/wp-content/uploads/2018/06/Mapadirect-svg-bleu.svg" alt="Image de MAPAdirect" height=150>
</a>

<div class="div-presentation-mapa">

<div>
<p class="presentation-mapa"> MAPAdirect est une startup Créée en 2016, c'est une Fintech qui permet à des marchands et à des collectivités 
    ou établissements publics de contractualiser de manière plus efficace et plus simple. Lauréat des « Trophées Blaise Pascal 
    de la commande publique », MAPAdirect propose des réponses innovantes aux enjeux des achats publics pour évoluer vers le 100% 
    numérique. Une « e-Galerie Marchande » dédiée aux acheteurs publics pour la fourniture de produits et services permet de simplifier 
    et fluidifier la passation de commandes.
</p>
</div>
<div class="div-localisation-mapa">
<iframe class="localisation-mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2623.1821292992418!2d2.0699042155562926!3d48.89286607929087!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e6884f49dcb765%3A0xecdf87186a39f9dd!2s21%20Avenue%20Saint-Fiacre%2C%2078100%20Saint-Germain-en-Laye!5e0!3m2!1sfr!2sfr!4v1589813623226!5m2!1sfr!2sfr" width="600" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
</div>



<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="margin-top: 10%;" height=10>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/image1mapa.jpg" class="d-block w-100" alt="..." height=550>
    </div>
    <div class="carousel-item">
      <img src="images/image2mapa.png" class="d-block w-100" alt="..." height=550>
    </div>
    <div class="carousel-item">
      <img src="images/image3mapa.png" class="d-block w-100" alt="..." height=550>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
   
    <p class="text-presentation-service"> Je suis dans le service informatique de la startup , qui est composé d'un directeur technique
    qui est également un des fondateurs de la startup , ensuite il y'a 2 alternants dont moi en bts et mon collégue en
    licence professionnel.

    </p>
       
    <p class="mission-mapa"> Les missions que j'ai effectué pour l'entreprise sont les suivantes : </br>
         - Création d'un site web avec HTML5 , CSS3 et jquery </br>
         - Gestion de produits sur le site marchand avec Prestashop </br>
             
    </p>
    




<?php
require 'inc/footer.php'
?>