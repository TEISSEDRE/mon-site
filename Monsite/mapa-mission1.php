<?php
require 'inc/header.php';
?>

<div class="background-mission-mapa-1">

<h1 class="titre-mapa-mission1"> Projet création du site MAPAdirect </h1>

<p class="contexte-mapa-mission1"> Contexte : Développer un site pour MAPAdirect </p>

<div class="barre-presentation-mapa-mission1">

</div>

<h4 class="objectif-mapa-mission1"> L'objectif était de créer un site de présentation pour MAPAdirect ou on peut s'inscrire à la marketplace et également suivre les
    actualités </h4>

<p class="langages-utilises"> Langages utilisés : HTML5 , CSS3 , PHP et une base de données avec SQL </p>

<img class="logo-mapa-mission1" src="images/mapalog.jpg" alt="Logo MAPAdirect">


<p class="text-page-accueil"> La page d'accueil </p>
<img class="image-page-accueil-mapa" src="images/mapa-accueil.png" alt="Image de la page d'accueil du site MAPAdirect">

<p class="text-page-inscription-mapa1"> L'inscription </p>
<img class="image-inscription-mapa" src="images/mapa-inscription.png" alt="Image de l'inscription de MAPAdirect">

<p class="text-actualite-mapa"> L'actualités de MAPA </p>
<img class="image-actu1-mapa" src="images/mapa-actu1.png" alt="Image de la 1ère page d'actualités MAPA">
<img class="image-actu2-mapa" src="images/mapa-actu2.png" alt="Image de la 2ème page d'actualités MAPA">

<p class="text-marketplace-mapa"> La Market place de MAPA </p>
<img class="image-marketplace-mapa1" src="images/mapa-marketplace.png" alt="Image de la market place de MAPA">

</div>

<?php
require 'inc/footer.php'
?>