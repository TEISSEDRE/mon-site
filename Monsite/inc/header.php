<!doctype html>

<html lang="fr">

<head>
  
  <meta charset="utf-8">
  
  <title>Site Robin Teissedre</title>
  <link rel="stylesheet" href="css/style.css">
  <script src="http://code.jquery.com/jquery-2.1.3.min.js" type="text/javascript"></script>
  <script src="js/script.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">

</head>

<body>
  
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    

    <nav class="navbar fixed-top navbar-light bg-light" style="background: linear-gradient(to right ,#C489F1, #5A188D);">
      <a class="navbar-brand" href="presentation.php">Accueil</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
        <ul class="navbar-nav mr-auto" style="background: linear-gradient(to right ,#C489F1, #5A188D);">
        <li class="nav-item dropdown" >
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Presentation de l'entreprise
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown" >
          <a class="dropdown-item" href="entreprise-1.php">Entreprise 1 ère année</a>
          <a class="dropdown-item" href="#">Entreprise 2 ème année</a>
        </div>
        
        
        <ul class="navbar-nav mr-auto" style="background: linear-gradient(to right ,#C489F1, #5A188D);">
        <li class="nav-item dropdown" >
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Situation professionnel
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown" >
          <a class="dropdown-item" href="situation_entreprise">En entreprise</a>
          <a class="dropdown-item" href="situation_ecole.php">A l'école</a>
        
          <li class="nav-item active">
            <a class="nav-link" href="veille.php">Veille technologique<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="#">Analyse critique<span class="sr-only">(current)</span></a>
          </li>
        </ul>
                
      </div>
    </nav>


 
    
 

