<?php
require 'inc/header.php';
?>
<div class="couleur-ppe-2">
<h1 class="titre-gsb-mutation"> Projet GSB demande des mutations </h1>

<p class="contexte-gsb-mutation"> Contexte : Développer une application en C# à destination des visiteurs médicaux afin qu'ils puissent gérer leurs demandes
de mutations </p>

<div class="barre-presentation">

</div>

<div class="div-page-connexion">

    <p class="texte-page-connexion"> La page d'accueil de l'application qui permet de se connecter soit en tant que visiteur soit
    en tant que RH. </p>

    
    

    <div>

      <img class="image-page-connexion" src="images/connexion-gsb.png" alt="Image de la page de connexion du projet GSB">
      <img class="image-connexion-ppe-2" src="images/connexion-ppe-2.png" alt="Image du code de la page de connexion du projet GSB">

    </div>

</div>

<input class="boutton-1-ppe" type="button" value="Voir un bout de code">

<script>
            
  $(document).ready(function() {
  $('.boutton-1-ppe').mouseenter(function() {
  $('.image-page-connexion').css("animation-play-state","running")
  $('.image-connexion-ppe-2').css("display","block")
  $('.image-connexion-ppe-2').css("animation-play-state","running")
  $('.boutton-1-ppe').css("display","none")
  });
  });



</script>


<div class="div-page-connexion">

    

    
    <img class="image-demande-affectation" src="images/demande-affectation-ppe-2.png" alt="Image du code de la page d'affectation visiteur'">
    <img class="image-page-affectation" src="images/affectation.png" alt="Image de la page d'affectation visiteur'">
    
    <div>

    <p class="texte-page-affectation"> Quand on se connecte en tant que visiteur nous avons accès à cette page qui permet de gérer
    les voeux , on peut en faire ou les modifier.
    Quand la RH aura validé l'affectation , le visiteur pourras voir ou il est affecté.  </p>
        
    </div>

</div>

<input class="boutton-2-ppe" type="button" value="Voir un bout de code">

<script>
            
  $(document).ready(function() {
  $('.boutton-2-ppe').mouseenter(function() {
  $('.image-page-affectation').css("animation-play-state","running")
  $('.image-demande-affectation').css("display","block")
  $('.image-demande-affectation').css("animation-play-state","running")
  $('.boutton-2-ppe').css("display","none")
  });
  });

  $(document).ready(function(){
  if ($('.image-page-affectation').css('opacity') === '0%') 
  {
    $('.image-page-affectation').css('display','none');
  }   

});


</script>

<div class="div-page-connexion">

    <p class="texte-page-gestion-voeux"> Quand on se connecte en tant que RH on arrive sur cette page ou on peut modifier le nombres
    de places. </p>

    

    <div>

      <img class="image-page-gestion-voeux" src="images/gestion-voeux.png" alt="Image de la page de gestion des voeux">
      <img class="image-modifierplaces-ppe-2" src="images/modifierplaces-ppe-2.png" alt="Image du code de la page de gestion des voeux">

    </div>

</div>

<input class="boutton-3-ppe" type="button" value="Voir un bout de code">

<script>

$(document).ready(function() {
$('.boutton-3-ppe').mouseenter(function() { 
$('.image-page-gestion-voeux').css("animation-play-state","running")
$('.image-modifierplaces-ppe-2').css("display","block")
$('.image-modifierplaces-ppe-2').css("animation-play-state","running")
$('.boutton-3-ppe').css("display","none")  
}); 
});

</script>


<div class="div-page-voeux-regions">

    
  <img class="image-page-affectation-voeux-code" src="images/affectationdesvoeux-ppe-2.png" alt="Image du code de la page d'affectation des voeux'"/>
  <img class="image-page-affectation-voeux" src="images/affectation-voeux.png" alt="Image de la page d'affectation des voeux'"/>
            
  <img class="image-page-affectation-régions" src="images/affectation-regions.png" alt="Image de la page d'affectation des régions'"/>
  <img class="image-page-affectation-régions-code" src="images/affectationregion-ppe-2.png" alt="Image du code de la page d'affectation des régions'"/>
    
</div>

<p class="texte-page-affectation-voeux-regions"> Ensuite en tant que RH on peut voir les voeux que l'on fait les visiteurs et les affecters. 
</p>

<input class="boutton-4-ppe" type="button" value="Voir un bout de code">

<script>

$(document).ready(function() {
$('.boutton-4-ppe').mouseenter(function() { 
$('.image-page-affectation-voeux').css("animation-play-state","running")
$('.image-page-affectation-voeux-code').css("display","block")
$('.image-page-affectation-voeux-code').css("animation-play-state","running")
$('.boutton-4-ppe').css("display","none")
$('.image-page-affectation-régions').css("animation-play-state","running")
$('.image-page-affectation-régions-code').css("display","block")
$('.image-page-affectation-régions-code').css("animation-play-state","running")  
}); 
});

 
</script>

</div>
<?php
require 'inc/footer.php'
?>